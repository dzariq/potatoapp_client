<?php namespace App\Http\Controllers\Api;

use Input;
use Cache;
use Response;
use Validator;
use Config;

use App\Models\Customer;
use App\Transformers\CustomerTransformer;

use App\Exceptions\NotFoundException;
use App\Exceptions\ResourceException;

class CustomerController extends ApiController
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $validator = Validator::make(Input::all(), [
            'ids'            => 'array|integerInArray',
            'page'           => 'integer',
            'created_at_min' => 'date_format:"Y-m-d H:i:s"',
            'created_at_max' => 'date_format:"Y-m-d H:i:s"',
            'updated_at_min' => 'date_format:"Y-m-d H:i:s"',
            'updated_at_max' => 'date_format:"Y-m-d H:i:s"',
            'limit'          => 'integer|min:1|max:250',
            'search'         => 'string',
            'role_ids'       => 'array|integerInArray'
        ]);
        if ($validator->fails()) {
            throw new ResourceException($validator->errors()->first());
        }

        $customers = new Customer;
        if (Input::has('ids')) {
            $customers = $customers->whereIn('id', Input::get('ids'));
        }
        //Filter
        if (Input::has('search')) {
            $customers = $customers->where('lastname', 'LIKE', '%' . Input::get('search') . '%')->orWhere('firstname', 'LIKE', '%' . Input::get('search') . '%');
        }

        $customers = $customers->simplePaginate(Input::get('limit', 50));

        return response()->paginator($customers, new CustomerTransformer);

    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {

        $customer = Customer::find($id);
        $this->checkExist($customer);

        return response()->item($customer, new CustomerTransformer);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        $rules = [
            'lastname'  => 'string|min:1|max:255',
            'firstname' => 'string|min:1|max:255',
            'active'    => 'boolean',
            'email'     => 'string|email',
            'phone'  	=> 'string|min:8|max:35',
            'address'	=> 'text|min:8|max:255',
			'sex'		=> 'string|min:1|max:1',
        ];
		
        $customer = new Customer;
        //$this->fillFieldFromInput($customer, ['active', 'email', 'password']);
        $this->fillNullableFieldFromInput($customer, ['lastname', 'firstname', 'email','phone','address','sex']);

        $customer->save();

        return $this->show($customer->id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function update($id)
    {
        $rules = [
            'lastname'  => 'string|min:1|max:255',
            'firstname' => 'string|min:1|max:255',
            'active'    => 'boolean',
            'email'     => 'string|email',
            'phone'  	=> 'string|min:8|max:35',
            'address'	=> 'text|min:8|max:255',
			'sex'		=> 'string|min:1|max:1',
        ];

        $validator = Validator::make(Input::only(array_keys($rules)), $rules);

        if ($validator->fails()) {
            throw new ResourceException($validator->errors()->first());
        }
        $customer = Customer::find($id);
        $this->checkExist($customer);

        //$this->fillFieldFromInput($user, ['active', 'email', 'password']);
        $this->fillNullableFieldFromInput($customer, ['lastname', 'firstname', 'email','phone','address','sex']);

        $customer->save();

        return $this->show($customer->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        $this->checkExist($customer);

        $customer->delete();

        return response()->return();
    }

}
